#ifndef CPPLOCK_H
#define CPPLOCK_H


#include <string>
#include <unistd.h>
#include <sys/file.h>


namespace cpplock {


using namespace std;


class FileLock {
public:
	int file_descriptor;
public:
	FileLock (string a_file_name, int mode) {
		file_descriptor = open (a_file_name.c_str (), O_RDONLY);
		flock (file_descriptor, mode);
	};
	virtual ~FileLock () {
		close (file_descriptor);
	};
};


class WriteLock: public FileLock {
public:
	WriteLock (string a_file_name):
		FileLock (a_file_name, LOCK_EX)
		{ };
};


class ReadLock: public FileLock {
public:
	ReadLock (string a_file_name):
		FileLock (a_file_name, LOCK_SH)
		{ };
};


}; /* namespace lock */


#endif /* #ifndef CPPLOCK_H */
