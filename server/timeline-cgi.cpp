#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "cpplock.h"


using namespace std;


int main (int argc, char *argv [])
{
	string filename {"/var/lib/zepam/timeline.cjson"};

	cpplock::ReadLock read_lock {filename};

	FILE *in = fopen (filename.c_str (), "r");
	assert (in);
	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer);

	cout << "Content-Type: text/plain" << endl << endl;

	for (; ; ) {
		auto result = fgets (buffer, 4096, in);
		if (result == nullptr) {
			break;
		}
		cout << buffer;
	}

	free (buffer);

	fclose (in);

	return 0;
}

