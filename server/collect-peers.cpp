#include <iostream>
#include <deque>
#include <string>
#include <cstdlib>

#include "picojson.h"
#include "cpplock.h"


using namespace std;


class Peer {
	string url;
	bool alive;
};


class InvalidJsonException: public exception {
};


static deque <Peer> peers;
static string file_name {"/var/lib/zepam/peers.cjson"};


static void read () {
	FILE *in = fopen (file_name.c_str (), "r");
	if (in == nullptr) {
		cerr << "Can not open " << file_name << endl;
		exit (1);
	}
	auto buffer_1 = static_cast <char *> (malloc (4096));
	if (buffer_1 == nullptr) {
		abort ();
	}
	string buffer_2;

	peers.clear ();

	for (; ; ) {
		auto fgets_return = fgets (buffer_1, 4096, in);
		if (fgets_return == nullptr) {
			break;
		}
		buffer_2 += string {buffer_1};
		if (buffer_2.back () == '\n') {
			try {
				picojson::value peer_value;
				auto parse_return = picojson::parse (peer_value, buffer_2);
				if (! parse_return.empty ()) {
					cerr << parse_return << endl;
					throw (InvalidJsonException {});
				}
				
				/* WIP */
				
			} catch (InvalidJsonException &e) {
				/* Do nothing. */
			}

			buffer_2.clear ();
		}
	}
}


static void step () {
	cpplock::WriteLock write_lock {file_name};
	read ();
}


int main ()
{
	step ();
	return 0;
}

